<?php

set_time_limit(9999999);

require_once 'twitteroauth/twitteroauth/twitteroauth.php';
require_once 'init.php';

define("FRIEND_PER_USER_LIMIT", 0);


if ($_POST) {
	if (isset($_POST["page"]) && isset($_POST["query"]) && trim($_POST["query"]) != "") {
		$str = str_replace("\n", " ", $_POST["query"]);
		$data = array();
		$query = array(
			"q" => $str,
			"count" => 100,
			"result_type" => "recent",
			"lang" => "en"
		);

		if ((int)$_POST["requestsAmount"] < $query["count"]) {
			$query["count"] = (int)$_POST["requestsAmount"];
		}

		if ($_POST["lastID"] != 0) {
			$query["max_id"] = $_POST["lastID"];
		}

		$results = search($query);
		if (gettype($results) == "object") {
			if (isset($results->errors)) {
				echo "Twitter API rate limit reached please wait 15 minutes";
			} else if (isset($results->statuses)) {
				$ids = array();
				$i = 1;
				foreach ($results->statuses as $result) {
					if (!in_array($result->id, $ids)) {
						$text = $result->text;
						$data[] = array(
							"id" => $result->id,
							"user_screen_name" => $result->user->screen_name,
							"name" => $result->user->name,
							"user_id" => $result->user->id,
							"location" => $result->user->location,
							"description" => $result->user->description,
							"url" => $result->user->url,
							"text" => $result->text,
							"created_at" => $result->created_at,
							"avatar" => $result->user->profile_image_url,
							"retweet_count" => $result->retweet_count,
							"geo" => $result->geo,
							"followers_count" => $result->user->followers_count,
							"friends_count" => $result->user->friends_count,
							"lang" => $result->user->lang,
							"source" => $result->source,
							"reply_to" => $result->in_reply_to_screen_name,
							"tweet_count" => $result->user->statuses_count
						);
						$ids[] = $result->id;
					}
					$i++;
				}
				echo json_encode(array("data" => $data, "amount" => $i));
			}else{
				echo "No data found";
			}
		} else {
			echo "Twitter API connection error, please try again later";
		}
	} else if (isset($_POST["createFile"])) {
		$result = array();
		$dirName = "tweet_data";
		$creation_stamp = time();
		$amount = 0;

		$unique_users = array();
		$screen_names = array();
		$number_of_followers = array();
		$number_of_friends = array();

		foreach ($_POST["data"] as $value) {
			$unique_users[$value["user_screen_name"]] = null;
			$screen_names[] = str_replace("@", "", $value["user_screen_name"]);
			$number_of_followers[] = $value["followers_count"];
			$number_of_friends[] = $value["friends_count"];
		}

		file_put_contents("{$dirName}/{$creation_stamp}_names.txt", json_encode($screen_names));
		file_put_contents("{$dirName}/{$creation_stamp}_followers_number.txt", json_encode($number_of_followers));
		file_put_contents("{$dirName}/{$creation_stamp}_friends_number.txt", json_encode($number_of_friends));

		$result["all_users_amount"] = count($unique_users);
		$result["creation_stamp"] = $creation_stamp;
		$result["screen_names"] = $creation_stamp . "_names.txt";
		$result["number_of_followers"] = $creation_stamp . "_followers_number.txt";
		$result["number_of_friends"] = $creation_stamp . "_friends_number.txt";

		echo json_encode($result);
	} else if (isset($_POST["createFriendFile"])) {
		$friends = array();
		$screen_names = array();
		$toa = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
		$dirName = "tweet_data";
		$friendsFileName = $_POST["file"] . "_friends.txt";
		$screenNamesFromTweetsFileName = $_POST["file"] . "_screen_names_from_tweets.txt";
		touch($dirName . "/" . $friendsFileName);
		touch($dirName . "/" . $screenNamesFromTweetsFileName);
		for ($j = 0; $j < count($_POST["data"]); $j++) {
			$screen_name = $_POST["data"][$j]["user_screen_name"];
			$screen_names[] = implode(",", get_screen_names_from_all_tweets($screen_name, 0));
			file_put_contents("{$dirName}/" . $screenNamesFromTweetsFileName, json_encode($screen_names));
			$friends[] = str_replace("@", "", implode(",", get_friends($screen_name, -1)));
			file_put_contents("{$dirName}/" . $friendsFileName, json_encode($friends));
		}
		echo json_encode(array("friends" => $friendsFileName, "screen_names" => $screenNamesFromTweetsFileName));
	} else if (isset($_POST["checkFriendsAmount"])) {
		$fileName = $_POST["file"] . "_friends.txt";
		$dirName = "tweet_data";
		$content = json_decode(file_get_contents($dirName . "/" . $fileName));
		$result = array("success" => true);
		if (gettype($content) == "array") {
			$result["amount"] = count($content);
		} else {
			$result["success"] = false;
		}
		echo json_encode($result);
	}
}

function get_friends($screen_name, $cursor) {
	global $toa;

	$result = array();
	$query = array(
		"screen_name" => $screen_name,
		"count" => 200,
		"cursor" => $cursor
	);
	$temp = $toa->get('friends/list', $query);
	if (isset($temp->errors)) {
		sleep(15 * 60);
		$result = array_merge($result, get_friends($screen_name, $cursor));
	} else {
		if ($temp->next_cursor != 0) {
			$result = array_merge($result, get_friends($screen_name, $temp->next_cursor));
		}
		foreach ($temp->users as $friend) {
			$result[] = $friend->screen_name;
		}
		if (FRIEND_PER_USER_LIMIT != 0 && count($result) >= FRIEND_PER_USER_LIMIT) {
			return $result;
		}
	}
	return $result;
}

function get_screen_names_from_all_tweets($screen_name, $max_id) {
	global $toa;

	$result = array();
	$query = array(
		"screen_name" => $screen_name,
		"count" => 200,
	);

	if ($max_id) $query["max_id"] = $max_id;

	$temp = $toa->get("statuses/user_timeline", $query);
	if (isset($temp->errors)) {
		sleep(15 * 60);
		$result = array_merge($result, get_screen_names_from_all_tweets($screen_name, $max_id));
	} else {
		if (count($temp) == 200) {
			$last_element = array_pop($temp);
			$last_tweet_id = $last_element->id;
			$result = array_merge($result, get_screen_names_from_all_tweets($screen_name, $last_tweet_id));
		}
		foreach ($temp as $tweet) {
			if ($tweet->id == $max_id) continue;
			preg_match_all('/@(\w+)/i', $tweet->text, $matches);
			if (count($matches) > 0 && isset($matches[1])) $result = array_merge($result, $matches[1]);
		}
	}
	return $result;

}