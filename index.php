<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

<title>Get Tweets</title>

<style>
	#searchWrapper {
		width: 300px;
		margin: 0 auto;
		padding: 5px;
		text-align: center;
	}

	.col-lg-6 {
		width: 100%;
	}

	td {
		padding: 5px;
		vertical-align: top;
	}

	.userData {
		width: 200px;
	}

	#linkP, #friendsLink, #screenNamesLink {
		display: none;
	}

	.progress {
		display: none;
	}

	.table-responsive {
		display: none;
	}

	#friendsLink button {
		width: 150px;
	}

	#linkP a {
		padding-left: 10px;
	}

</style>

<script>
$(function () {

	$.fn.pageMe = function (opts) {
		var $this = this,
			defaults = {
				perPage: 100,
				showPrevNext: false,
				hidePageNumbers: false
			},
			settings = $.extend(defaults, opts);

		var listElement = $this;
		var perPage = settings.perPage;
		var children = listElement.children();
		var pager = $('.pager');

		if (typeof settings.childSelector != "undefined") {
			children = listElement.find(settings.childSelector);
		}

		if (typeof settings.pagerSelector != "undefined") {
			pager = $(settings.pagerSelector);
		}

		var numItems = children.size();
		var numPages = Math.ceil(numItems / perPage);

		pager.data("curr", 0);

		if (settings.showPrevNext) {
			$('<li><a href="#" class="prev_link">«</a></li>').appendTo(pager);
		}

		var curr = 0;
		while (numPages > curr && (settings.hidePageNumbers == false)) {
			$('<li><a href="#" class="page_link">' + (curr + 1) + '</a></li>').appendTo(pager);
			curr++;
		}

		if (settings.showPrevNext) {
			$('<li><a href="#" class="next_link">»</a></li>').appendTo(pager);
		}

		pager.find('.page_link:first').addClass('active');
		pager.find('.prev_link').hide();
		if (numPages <= 1) {
			pager.find('.next_link').hide();
		}
		pager.children().eq(1).addClass("active");

		children.hide();
		children.slice(0, perPage).show();

		pager.find('li .page_link').click(function () {
			var clickedPage = $(this).html().valueOf() - 1;
			goTo(clickedPage, perPage);
			return false;
		});
		pager.find('li .prev_link').click(function () {
			previous();
			return false;
		});
		pager.find('li .next_link').click(function () {
			next();
			return false;
		});

		function previous() {
			var goToPage = parseInt(pager.data("curr")) - 1;
			goTo(goToPage);
		}

		function next() {
			var goToPage = parseInt(pager.data("curr")) + 1;
			goTo(goToPage);
		}

		function goTo(page) {
			var startAt = page * perPage,
				endOn = startAt + perPage;

			children.css('display', 'none').slice(startAt, endOn).show();

			if (page >= 1) {
				pager.find('.prev_link').show();
			}
			else {
				pager.find('.prev_link').hide();
			}

			if (page < (numPages - 1)) {
				pager.find('.next_link').show();
			}
			else {
				pager.find('.next_link').hide();
			}
			pager.data("curr", page);
			pager.children().removeClass("active");
			pager.children().eq(page + 1).addClass("active");
		}
	};


	var data = [],
		amount = 0,
		ids = [],
		requestsAmount = 100,
		defaultRequestsAmount = 3,
		requestsMade = 0,
		lastID = 0,
		sendRequest = function (j) {
			if (j == 1) {
				amount = 0;
				ids = [];
				data = [];
				$("#linkP, #friendsLink, .progress, #screenNamesLink").hide();
				$("#progress-bar").css({width: 0}).attr({"aria-valuenow": 0});
				$("#tweetsAmount").html("Loading ...");
				$("#myTable").remove();
				$("#resultTable").append("<tbody id='myTable'></tbody>");
				$("#myPager").html("");
				$(".table-responsive").show();
			} else if (requestsMade >= 10 && amount == 0) {
				$("#tweetsAmount").html(" Could not find any data ...");
				return false;
			}
			$("#tweetsAmount").html(amount);
			if (amount < requestsAmount) {
				$.ajax({
					type: "post",
					url: "ajax.php",
					data: {page: j, query: $("#keywords").val(), lastID: lastID, requestsAmount: requestsAmount},
					dataType: "json",
					success: function (res) {
						if (res.data[0] != undefined) {
							lastID = res.data[(res.data.length - 1)]["id"];
						} else {
							amount = requestsAmount;
						}
						requestsMade++;
						for (var key in res.data) {
							if (res.data.hasOwnProperty(key)) {
								if (ids.indexOf(res.data[key]["id"]) == -1) {
									data.push(res.data[key]);
									ids.push(res.data[key]["id"]);
									amount++;
								}
							}
						}
						sendRequest(++j);
					},
					error: function (res) {
						alert(res.responseText);
					}
				})
			} else {
				var str = "";
				for (var key in data) {
					if (data.hasOwnProperty(key)) {
						str += "<tr><td>" +
							"<table><tr>" +
							"<td class='userData'>" + data[key]["user_screen_name"] + "<br />" +
							"<img src='" + data[key]["avatar"] + "'/><br />" +
							"<b>name</b>: " + data[key]["name"] + "; <br />" +
							"<b>tweets</b>: " + data[key]["tweet_count"] + "; <br />" +
							"<b>lang</b>: " + data[key]["lang"] + "; <br />" +
							"<b>friends:</b> " + data[key]["friends_count"] + ";<br />" +
							"<b>followers:</b> " + data[key]["followers_count"] + "<br />" +
							"<b>location:</b> " + data[key]["location"] + "<br />" +
							"<b>description:</b> " + data[key]["description"] + "<br />" +
							"<b>url:</b> " + ( (data[key]["url"] != null) ? " <a href='" + data[key]["url"] + "'>" + data[key]["url"] : " " ) + "</a><br />" +
							"<b>id:</b> " + data[key]["id"] +
							"</td>" +
							"<td>Created at: " + data[key]["created_at"] + "; id: " + data[key]["id"] +
							"<hr />" +
							data[key]["text"] + "<br />" +
							"<hr /> geo: " + ( (data[key]["geo"] != null) ? data[key]["geo"]["coordinates"][0] + "," + data[key]["geo"]["coordinates"][1] : "  " ) + "; retweets: " + data[key]["retweet_count"] +
							"<hr />source: " + data[key]["source"] + "; reply to: " + data[key]["reply_to"] +
							"</td>" +
							"<tr></table>" +
							"</td></tr>";
					}
				}
				$("#tweetsAmount").html(amount);
				$("#myTable").html(str);
				$('#myTable').pageMe({pagerSelector: '#myPager', showPrevNext: true, hidePageNumbers: false, perPage: 100});

				$.ajax({
					type: "post",
					url: "ajax.php",
					data: {createFile: true, data: data},
					dataType: "json",
					success: function (res) {
						var result = res,
							checkFriends,
							loadingIndicator,
							html = "",
							clearIntervals = function () {
								clearInterval(loadingIndicator);
								clearInterval(checkFriends);
							};

						$("#linkP, #friendsLink, .progress").show();

						$("#fileLink").html(
							"<a href='/tweet_data/" + res.screen_names + "' download='names'><button type='button' class='btn btn-primary'>names</button></a>" +
								"<a href='/tweet_data/" + res.number_of_followers + "' download='followers_amount'><button type='button' class='btn btn-primary'>followers amount</button></a>" +
								"<a href='/tweet_data/" + res.number_of_friends + "' download='friends_number'><button type='button' class='btn btn-primary'>friends amount</button></a>"
						);

						$("#friendsLink").html("<a href='#'><button type='button' class='btn btn-primary' disabled='disabled'>Loading friends <span id='loading_indicator'></span></button></a>");

						loadingIndicator = setInterval(function () {
							if (html == "....") html = "";
							$("#loading_indicator").html(html);
							html += ".";
						}, 1000);

						$.ajax({
							type: "post",
							url: "ajax.php",
							data: {createFriendFile: true, data: data, file: res.creation_stamp},
							dataType: "json",
							success: function (res) {
								clearIntervals();
								$("#friendsLink").html("<a href='/tweet_data/" + res.friends + "' download='friends'><button type='button' class='btn btn-primary'>friends</button></a>");
								$("#screenNamesLink").show().html("<a href='/tweet_data/" + res.screen_names + "' download='screen_names'><button type='button' class='btn btn-primary'>screen names</button></a>");
								checkFile(result);
							},
							error: function (res) {
								clearIntervals();
								checkFile(result);
								alert(res.responseText);
							}
						});
						checkFriends = setInterval(function () {
							checkFile(res);
						}, (30 * 1000));
					},
					error: function (res) {
						alert(res.responseText);
					}
				});
			}
		};

	$("#searchBtn").click(function () {
		var v = parseInt($("#requestPagesAmount").val());

		requestsAmount = (!isNaN(v)) ? v : defaultRequestsAmount;
		if ($("#keywords").val().trim() != "") {
			sendRequest(1);
		} else {
			alert("Need keywods to search!")
		}
	});

	function checkFile(result) {
		var percent = 0;

		$.ajax({
			type: "post",
			url: "ajax.php",
			data: {checkFriendsAmount: true, file: result.creation_stamp},
			dataType: "json",
			success: function (res) {
				if (res.success) {
					percent = parseInt((res.amount / result.all_users_amount) * 100);
					$("#progress-bar").attr({"aria-valuenow": percent}).css({width: percent + "%"});
				}
			},
			error: function (res) {
				console.log("error", res.responseText);
			}
		});
	}
});
</script>
</head>
<body>
<div id="searchWrapper">
	<p>
		<label for="keywords">Keywords</label>
		<textarea name="keywords" id="keywords" cols="5" rows="3" class="form-control" placeholder="Enter keywords"></textarea>
	</p>

	<div class="row">
		<div class="col-lg-6">
			<div class="input-group">
				<input id="requestPagesAmount" type="text" class="form-control" placeholder="Enter amount here">
				  <span class="input-group-btn">
					<button class="btn btn-default" type="button" id="searchBtn">Search</button>
				  </span>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="table-responsive">
			<h4>Tweets amount: <span id="tweetsAmount"></span></h4>

			<p id="linkP"><span id="fileLink"></span><span id="friendsLink"></span><span id="screenNamesLink"></span></p>

			<div class="progress">
				<div id="progress-bar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
			</div>
			<table class="table table-hover" id="resultTable">
			</table>
		</div>
		<div class="col-md-12 text-center">
			<ul class="pagination pagination-lg pager" id="myPager"></ul>
		</div>
	</div>
</div>
</body>
</html>